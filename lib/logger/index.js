var _ = require('lodash');

var Logger = function(debugLevel, name) {
	this.level = null;
	this.name = name;
	this.defaultLevel = 'info';
	this.debugLevel = debugLevel;
};

Logger.prototype._log = function(level, messages)
{
	this.checkLevel();

	var output = [];
	if (!_.isObject(messages)) {
		messages = {0: messages};
	}
	for (var i in messages) {
		if (!_.isString(messages[i])) {
			output.push(messages[i] instanceof Error ? messages[i].toString() : JSON.stringify(messages[i]));
		} else {
			output.push(messages[i]);
		}
	}

	var date = new Date();
	var month = date.getMonth() + 1;
	month = (month < 10 ? '0' : '') + month;
	var day = date.getDate();
	day = (day < 10 ? '0': '') + day;
	var hour = date.getHours();
	hour = (hour < 10 ? '0' : '') + hour;
	var minutes = date.getMinutes();
	minutes = (minutes < 10 ? '0' : '') + minutes;
	var seconds = date.getSeconds();
	seconds = (seconds < 10 ? '0' : '') + seconds;
	var milliseconds = date.getMilliseconds();
	var time = day + '.' + month + '.' + date.getFullYear() + ' ' + hour + ':' + minutes + ':' + seconds + '.' + milliseconds;
	var letter = level.substring(0, 1).toUpperCase();

	var logMessage = '[' + time + ']['+letter+']['+this.name+']: ' + output.join(' ');

	(console[level] && console[level] || console['log'])(logMessage);
};

/**
 * Set current loglevel
 */
Logger.prototype.checkLevel = function()
{
	if(!this.level || this.level != this.debugLevel)
	{
		this.level = this.debugLevel || this.defaultLevel;
		//this.info('Log level:', this.level);
	}
};
/**
 * Short level methods
 */

Logger.prototype.info = function()
{
	this._log('info', arguments);
};
Logger.prototype.debug = function()
{
	this._log('debug', arguments);
};
Logger.prototype.warn = function()
{
	this._log('warn', arguments);
};
Logger.prototype.error = function()
{
	this._log('error', arguments);
};
Logger.prototype.log = function()
{
	this._log('log', arguments);
};

module.exports = function(name) {
	return new Logger('debug', name);
};