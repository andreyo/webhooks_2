import {argv} from 'optimist';
import path from 'path';
import fs from 'fs';
var log = require('lib/logger')('CONFIG_LOADER');

class ConfigLoader {
	constructor(opts) {
		this.env = argv.env;
		this.filePath = path.resolve(__dirname, './../../configs', process.env.APP_CONFIG + '.json');
		this.config = {};

		this.init();
	}

	init() {
		this.config = this.readFile();
	}

	readFile() {
		let data = fs.readFileSync(this.filePath),
			parsed = {};

		try {
			parsed = JSON.parse(data);
		} catch(e){
			log.error('config parse error');
		}

    	log.info(`ENV: ${process.env.NODE_ENV}, CONFIG: ${process.env.APP_CONFIG}`);

		return parsed;
	}
}

module.exports = ConfigLoader;
