import amqplib from 'amqplib';

//import mediator from 'src/mediator';
//import _ from 'lodash';
//import async from 'async';
var log = require('lib/logger')('TRANSPORT_AMQP');

export default class AMQP {
  constructor() {
    this.config = {};
    this.client = null;
    this.conn = null;
  }

  init(options, callback) {
    this.config = options;

    return callback();

    const uri = ['amqp://', this.config.user, ':', this.config.password, '@', this.config.host, ':', this.config.port, '/', this.config.vhost].join('');
    this.client = amqplib.connect(uri, {});
    this.client.then(this._onOpen.bind(this, callback));

    /*this.io = new SocketIO(this.config.port, {
      transports: this.config.transports
    });

    this.io.on('connection', this._onConnect.bind(this));
    mediator.on('ws:emit', this._onEmit.bind(this));
    mediator.on('ws:broadcast', this._onBroadcast.bind(this));*/
  }

  _onOpen(callback, conn) {
    conn.on('close', this._onClose.bind(this));
    conn.on('error', this._onError.bind(this));

    this.config.channels.forEach((item, name) => {
      conn.createChannel((err, ch) => {
        conn.assertQueue('tasks', {
          durable: true,
          autoDelete: false
        });
      });
    });

    callback();
  }

  _onClose() {
    log.info('connection closed');
  }

  _onError() {
    log.error('ERROR', arguments);
  }

  destroy() {
    this.conn && this.conn.close();
  }

  getInstance() {
    return this.client;
  }
}