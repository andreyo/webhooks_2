var path = require('path');
process.env.BABEL_DISABLE_CACHE = 1;
process.env.NODE_PATH = path.join(__dirname, '.');
require('module').Module._initPaths();
require('babel-register');
new (require('./app'))();