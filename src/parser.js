import _ from 'lodash';

export default class Parser {
  constructor(request) {
    this.request = request;

    return this.parse(this.request);
  }

  parse(data) {
    return new Promise((resolve, reject) => {
      let obj = {
        commands: [],
        callback: false
      };

      if (!_.isArray(data.commands))
        throw new Error('commands must be an array', 400);

      if (data.commands.length == 0)
        throw new Error('commands array is empty');

      if (data.callback && _.isBoolean(data.callback) === false)
        throw new Error('callback must be a boolean');
      else
        obj.callback = data.callback;

      for (let i in data.commands) {
        if (data.commands.hasOwnProperty(i)) {
          let command = data.commands[i];

          if (command.delay && !_.isNumber(command.delay))
            throw new Error(`commands[${i}].delay must be a number`);

          if (command.faultDelay && !_.isNumber(command.faultDelay))
            throw new Error(`commands[${i}].faultDelay must be a number`);

          if (command.faultAttempts && !_.isNumber(command.faultAttempts))
            throw new Error(`commands[${i}].faultAttempts must be a number`);

          obj.commands.push({
            proxy: command.proxy,
            delay: command.delay || 0,
            faultDelay: command.faultDelay || 1000,
            faultAttempts: command.faultAttempts || 1
          });
        }
      }

      resolve(obj);
    });
  }
}
