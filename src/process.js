import _ from 'lodash';
import async from 'async';
import request from 'request';

var log = require('lib/logger')('PROCESS');

export default class Process {
  constructor(id, command, catcher, callback) {
    this.id = id;
    this.command = command;
    this.catcher = catcher;
    this.callback = callback;

    this.run();
  }

  run() {
    log.debug(` |- --> Process #${this.id} started with delay:`, this.command.delay || 0, '; command', this.command);
    _.delay(() => {
      async.retry({
          times: this.command.faultAttempts,
          interval: this.command.faultDelay
        },
        this.doRequest.bind(this),
        (err, result) => {
          if (err) {
            log.error(` |- <-- Process #${this.id} done with errors: `, err);
          } else {
            log.debug(` |- <-- Process #${this.id} done: `, result);
          }
          if (this.callback) {
            this.callback(null, result);
          }
        }
      )
    }, this.command.delay);
  }

  doRequest(callback) {
    request({
      method: this.catcher.method,
      uri: this.catcher.uri,
      body: JSON.stringify(this.command.proxy),
      headers: {
        'Content-type': 'application/json'
      },
      timeout: 60 * 1000
    }, (err, response, body) => {
      let result = {
        statusCode: response && response.statusCode || null,
        body: body || null
      };

      if (err || response.statusCode !== 200) {
        return callback('REQUEST_ERROR', result);
      }
      return callback(null, result);
    });
  }
}
