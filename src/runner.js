import _ from 'lodash';
import async from 'async';
import request from 'request';

import Process from './process';

var log = require('lib/logger')('RUNNER');

var runnerId = 0;

export default class Runner {
  constructor(data, catcher) {
    this.commands = data.commands;
    this.catcher = catcher;
    this.id = ++runnerId;

    return this.run();
  }

  run() {
    return new Promise((resolve, reject) => {
      let i = 0;
      log.debug(`Runner #${this.id} started`);
      async.map(this.commands, (command, cb) => {
        let pid = (this.id + '-' + i++);
        new Process(pid, command, this.catcher, cb);
      }, (err, result) => {
        if (err) {
          log.error(`Runner #${this.id} finished with errors: `, err);
          return reject(err);
        }
        log.debug(`Runner #${this.id} finished`);
        return resolve(result);
      });
    });
  }
}
