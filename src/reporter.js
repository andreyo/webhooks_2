import _ from 'lodash';
import async from 'async';
import request from 'request';

import Process from './process';

var log = require('lib/logger')('REPORTER');

var reporterId = 0;

export default class Runner {
  constructor(data, reporter) {
    this.data = data;
    this.reporter = reporter;
    this.id = ++reporterId;

    return this.run();
  }

  run() {
    return new Promise((resolve, reject) => {
      let pid = (this.id + '-0');
      log.debug(`Reporter #${this.id} started`);
      new Process(pid, this.data, this.reporter, (err, result) => {
        if (err) {
          log.error(`Reporter #${this.id} finished with errors: `, err);
          return reject(err);
        }
        log.debug(`Reporter #${this.id} finished`);
        return resolve(result);
      });
    });
  }
}
