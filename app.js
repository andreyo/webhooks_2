var log = require('lib/logger')('APP');
import ConfigLoader from 'lib/config-loader';
import Parser from 'src/parser';
import Runner from 'src/runner';
import Reporter from 'src/reporter';
import co from 'co';
import _ from 'lodash';

const appConfig = new ConfigLoader().config;

var app = require('koa')();
var koaBody = require('koa-body')();
var router = require('koa-router')();

app.use(koaBody);
app.use(function *(next){
  try {
    this.requestBody = _.isObject(this.request.body) && this.request.body || JSON.parse(this.request.body);
    yield next;
  } catch (e) {
    this.body = {
      error: "Invalid JSON"
    };
    this.status = 400;
  }
});

router.post('/batter', function *(next) {
  let data = yield new Parser(this.requestBody);
  let config = appConfig.components.platform;

  this.body = {
    result: 'OK'
  };
  this.status = 200;
  yield next;

  co(function *(){
    let runnerResult = yield new Runner(data, config.catcher);
    if (data.callback === true) {
      let reporterResult = yield new Reporter(runnerResult, config.reporter);
    }
  });

});

router.get('/notification', function *(next) {
  this.body = 'Coming soon...';
  yield next;
});

app
  .use(router.routes())
  .use(router.allowedMethods());

app.on('error', function(err){
  log.error('error', err.stack);
});

app.use(function *(next) {
  try {
    yield next;
  }
  catch (e) {
    this.body = {
      error: err && err.message
    };
    this.status = 400;
  }
});

app.use(function *(next) {
  yield next;

  if (404 != this.status) return;

  this.status = 404;

  switch (this.accepts('html', 'json')) {
    case 'html':
      this.type = 'html';
      this.body = '<p>Page Not Found</p>';
      break;
    case 'json':
      this.body = {
        message: 'Page Not Found'
      };
      break;
    default:
      this.type = 'text';
      this.body = 'Page Not Found';
  }
});

app.listen(appConfig.transports.server.port);

module.exports = function(){};
